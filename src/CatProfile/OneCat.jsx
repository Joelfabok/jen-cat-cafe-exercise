import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

const OneCat = (props) => {
  console.log(props);
  let id = props.match.params.id;
  const [cats, setCats] = useState([]);
  const [form, setForm] = useState({ display: "none" });
  const [cat, setCat] = useState({ name: "", image: "" });
  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:3001/api/cats/${id}`);
      res.json().then((res) => setCats(res));
    }
    fetchData();
  }, [id]);

  const handleDelete = (event, id) => {
    event.preventDefault();
    console.log("jen was here");
    console.log(id);

    fetch(`/api/cats/${id}`, {
      method: "delete",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
    history.push("/cats");
  };

  const handleEdit = (event, cat) => {
    event.preventDefault();
    console.log("jen is here again");
    console.log(cat);
    setForm({ display: "block" });
    setCat(cat);
  };

  const handleChange = (event) => {
    setCat((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = (event, id) => {
    event.preventDefault();
    console.log(id);
    fetch(`/api/cats/${id}`, {
      method: "put",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },

      //make sure to serialize your JSON body
      body: JSON.stringify(cat),
    }).then((response) => response.json());
    history.push("/cats");
  };


  return (
    <div>
      {cats.map((cat) => (
        <div key={cat.id}>
          <p>{cat.name}</p>
          <img src={cat.image} alt={cat.name} width="300" height="300" />
          <button
            onClick={(e) => {
              handleDelete(e, cat.id);
            }}
          >
            Delete Me!
          </button>
          <button
            onClick={(e) => {
              handleEdit(e, cat);
            }}
          >
            Edit Me!
          </button>
        </div>
      ))}


{/* This is the form that pops up when you press the Edit Me! button */}
      <form onSubmit={(e) => handleSubmit(e, cat.id)} style={form}>
        <div>
          <label>
            Name:
            <input
              type="text"
              name="name"
              value={cat.name}
              onChange={handleChange}
            />
          </label>
        </div>
        <div>
          <label>
            Photo:
            <input
              type="text"
              name="image"
              value={cat.image}
              onChange={handleChange}
            />
          </label>
        </div>
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
};

export default OneCat;
